/**
 * Created by yishuangxi on 2015/11/17.
 */

(function (window, $) {
    if (window.Tetris) {
        return;
    }
    Tetris.randomType = function () {
    };
    Tetris.randomStatus = function () {
    };
    function Tetris(config) {
        var defaults = {
            container: $('#container'),
            grid: {
                columns: 10,
                rows: 20,
                height: 20,
                width: 20
            }
        };
        this.config = $.extend({}, defaults, config);
        this.container = config.container;
        this.init();
        this.bindEvents();
    }

    Tetris.prototype.init = function () {
        var blocks_arr = [];
        for (var r = 0; r < this.config.grid.rows; r++) {
            var rows = [];
            for (var c = 0; c < this.config.grid.columns; c++) {
                rows.push('<td id="id_' + c + '_' + r + '" class="grid" style="height:' + this.config.grid.height + 'px;width:' + this.config.grid.width + 'px"></td>');
            }
            blocks_arr.push('<tr class="row">' + rows.join('') + '</tr>');
        }
        var table = $('<table>' + blocks_arr.join('') + '</table>');
        this.container.append(table);
    };

    Tetris.prototype.start = function () {
        var self = this;
        setInterval(function(){
            if (!self.brick) {
                var status = 0,
                    type = Brick.types.I;
                self.brick = new Brick({type: type, status: status});
                //这里需要根据不同类型的brick，以及brick不同的状态，设置不同的初始坐标
                self.brick.render({x: (self.config.grid.columns - 4) / 2, y: -4});
            }
            if(!self.isBorderOutBound(self.brick, Brick.directions.DOWN)){
                self.brick.move(Brick.directions.DOWN);
            }else{
                self.brick = null;
                self.eliminate();
            }
        }, 1000);
    };
    Tetris.prototype.end = function () {
    };
    Tetris.prototype.restart = function () {
        this.end();
        this.start();
    };
    Tetris.prototype.pause = function () {
    };
    Tetris.prototype.continue = function () {
    };
    Tetris.prototype.bindEvents = function () {
        var self = this;
        var KEY_UP = 38, KEY_DOWN = 40, KEY_LEFT = 37, KEY_RIGHT = 39;
        $(document).keydown(function (event) {
            if (!self.brick) return;

            if (event.which === KEY_UP) {
                if(self.isRotateOutBound(self.brick)) return;
                self.brick.rotate();
            } else if (event.which === KEY_DOWN) {
                if (self.isBorderOutBound(self.brick, Brick.directions.DOWN)) return;
                self.brick.move(Brick.directions.DOWN);
            } else if (event.which === KEY_LEFT) {
                if (self.isBorderOutBound(self.brick, Brick.directions.LEFT)) return;
                self.brick.move(Brick.directions.LEFT);
            } else if (event.which === KEY_RIGHT) {
                if (self.isBorderOutBound(self.brick, Brick.directions.RIGHT)) return;
                self.brick.move(Brick.directions.RIGHT);
            } else {

            }
        });
    };

    //判断是否已经到左/右/下/边界或者左右下已经有brick挡住了
    Tetris.prototype.isBorderOutBound = function (brick, direction) {
        var absCoordinates = brick.getAbsCoordinates();
        var cloneAbsCoordinates = $.extend(true, [], absCoordinates);
        //首先判断左边是否已经超出边界
        if (direction === Brick.directions.LEFT) {
            //选出Brick中所有最左边的block坐标:每一个坐标都和前面所有的坐标挨个比较,如果有y值相同的,则比较其x值,删除x值较大(更靠右边)的那个坐标
            for (var i = 1; i < cloneAbsCoordinates.length; i++) {
                for (var k = 0; k < i; k++) {
                    if (cloneAbsCoordinates[k].y === cloneAbsCoordinates[i].y) {
                        if (cloneAbsCoordinates[k].x > cloneAbsCoordinates[i].x) {
                            cloneAbsCoordinates.splice(k, 1);
                        } else {
                            cloneAbsCoordinates.splice(i, 1);
                        }
                        //因为删除了一个元素,所以要把i值往回走一个位置
                        i--;
                    }
                }

            }
            for (var i = 0; i < cloneAbsCoordinates.length; i++) {
                var leftX = cloneAbsCoordinates[i].x - 1;
                if (leftX < 0 || $('#id_' + leftX + '_' + cloneAbsCoordinates[i].y).hasClass('block')) {
                    return true;
                }
            }
        }
        else if (direction === Brick.directions.RIGHT) {
            //选出Brick中所有最右边的block坐标:每一个坐标都和前面所有的坐标挨个比较,如果有y值相同的,则比较其x值,删除x值较小(更靠左边)的那个坐标
            for (var i = 1; i < cloneAbsCoordinates.length; i++) {
                for (var k = 0; k < i; k++) {
                    if (cloneAbsCoordinates[k].y === cloneAbsCoordinates[i].y) {
                        if (cloneAbsCoordinates[k].x < cloneAbsCoordinates[i].x) {
                            cloneAbsCoordinates.splice(k, 1);
                        } else {
                            cloneAbsCoordinates.splice(i, 1);
                        }
                        //因为删除了一个元素,所以要把i值往回走一个位置
                        i--;
                    }
                }

            }
            for (var i = 0; i < cloneAbsCoordinates.length; i++) {
                var rightX = cloneAbsCoordinates[i].x + 1;
                if (rightX > (this.config.grid.columns - 1) || $('#id_' + rightX + '_' + cloneAbsCoordinates[i].y).hasClass('block')) {
                    return true;
                }
            }
        }
        else if (direction === Brick.directions.DOWN) {
            //选出Brick中所有最下边的block坐标:每一个坐标都和前面所有的坐标挨个比较,如果有x值相同的,则比较其y值,删除y值较小(更靠上边)的那个坐标
            for (var i = 1; i < cloneAbsCoordinates.length; i++) {
                for (var k = 0; k < i; k++) {
                    if (cloneAbsCoordinates[k].x === cloneAbsCoordinates[i].x) {
                        if (cloneAbsCoordinates[k].y < cloneAbsCoordinates[i].y) {
                            cloneAbsCoordinates.splice(k, 1);
                        } else {
                            cloneAbsCoordinates.splice(i, 1);
                        }
                        //因为删除了一个元素,所以要把i值往回走一个位置
                        i--;
                    }
                }

            }
            for (var i = 0; i < cloneAbsCoordinates.length; i++) {
                var downY = cloneAbsCoordinates[i].y + 1;
                if (downY > (this.config.grid.rows - 1)||$('#id_' + cloneAbsCoordinates[i].x + '_' + downY).hasClass('block')) {
                    return true;
                }
            }
        }

        return false;
    };

    Tetris.prototype.isRotateOutBound = function(brick){
        var absCoordinate = brick.getAbsCoordinates();
        var leftBorderIndex = 0,
            rightBorderIndex = this.config.grid.columns - 1,
            downBorderIndex = this.config.grid.rows - 1;
        if(brick.type === Brick.types.I){
            if(brick.status === Brick.status.A || brick.status === Brick.status.C ){
                var center = absCoordinate[2];
                //如果旋转之后左右两边越界了,则不执行旋转操作
                if ((center.x - 1) < leftBorderIndex || (center.x + 1) > rightBorderIndex || (center.x + 2) > rightBorderIndex) return true;
                //如果旋转时有block挡住了,也不执行旋转操作
                for(var y = 0; y < 2; y++){
                    if($('#id_' + (center.x - 1) + '_' + (center.y + y)).hasClass('block')) return true;
                }
                for(var x = 1; x < 3; x++){
                    for(var y = 0; y < 3; y++){
                        if($('#id_' + (center.x+x) + '_' + (center.y - y)).hasClass('block')) return true;
                    }
                }
            }else if(brick.status === Brick.status.B || brick.status === Brick.status.D ){
                var center = absCoordinate[1];
                //如果旋转之后下边越界了,或者下一个空位已经有block占位了,则不执行旋转操作
                if ((center.y + 1) > downBorderIndex || $('#id_' + center.x + '_' + (center.y + 1)).hasClass('block')) return true;
            }
        }else if(brick.type === Brick.types.J){
            
        }

        return false;
    };

    Tetris.prototype.eliminate = function(){
        var $rows = $('.row');
        for(var i = 0; i < $rows.length; i++){
            var $row = $rows.eq(i);
            if($row.find('.block').length === this.config.grid.columns){
                $row.find('.block').removeClass('block');
            }
        }
    }

    window.Tetris = Tetris;
})(window, jQuery);