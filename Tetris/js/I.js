/**
 * Created by yishuangxi on 2015/11/18.
 */
(function($){
    var Tetris = window.Tetris;

    Tetris._createI = function (status, positions) {
        var status = status || 0;
        var brick = {
            type:Tetris.types.I,//brick类型
            status: status,//brick状态
            positions:positions,//brick位置
            elements:[]//砖块元素
        };
        return brick;
    };

    Tetris._renderI = function (brick) {
        var status = brick.status;
        var positions = brick.positions;
        var elements = brick.elements;
        //渲染之前，要先清理之前的block
        if(elements.length > 0){
            for(var i = 0; i < brick.elements.length; i++){
                brick.elements[i].removeClass('block');
            }
            elements = [];
        }
        //渲染的brick的依据是：brick的position参数和brick的status：position决定了在哪里渲染，status决定了渲染成什么样的形状
        if(status === Tetris.status.A || status === Tetris.status.C){
            for(var i = 0; i < positions.length; i++){
                elements.push($('#id_' + (positions[i].x) + '_' + (position[i].y)));
                elements[i].addClass('block');
            }
        }else if(status === Tetris.status.B || status === Tetris.status.D){

        }else{
            throw Error('没有这个状态：status '+status);
        }

        return brick;
    };

    Tetris._rotateI = function(brick){
        var status = brick.status;
        var coordinates = brick.coordinates;
        var new_coordinates = [];
        var position = brick.position;
        //根据不同的状态，生成相应的坐标
        if(status == 1 || status == 3){
            new_coordinates[0] = [coordinates[0][0] + 1, coordinates[0][1] - 2];
            new_coordinates[1] = [new_coordinates[0][0], new_coordinates[0][1]+1];
            new_coordinates[2] = [new_coordinates[0][0], new_coordinates[0][1]+2];
            new_coordinates[3] = [new_coordinates[0][0], new_coordinates[0][1]+3];
        }else{
            new_coordinates[0] = [coordinates[0][0] - 1, coordinates[0][1] + 2];
            new_coordinates[1] = [new_coordinates[0][0] + 1, new_coordinates[0][1]];
            new_coordinates[2] = [new_coordinates[0][0] + 2, new_coordinates[0][1]];
            new_coordinates[3] = [new_coordinates[0][0] + 3, new_coordinates[0][1]];
        }
        //对新坐标进行越界判断，如果越界，则操作取消
        if(Tetris._isOutBounds(new_coordinates, position)) return;
        //更新坐标，更新状态
        brick.coordinates = new_coordinates;
        brick.status = (status + 1)%4;
        //重新渲染
        Tetris.renderBrick(brick);
        return brick;
    }
})(jQuery);