/**
 * Created by yishuangxi on 2015/11/17.
 */

(function (window, $) {
    if (window.Tetris) {
        return;
    }
    var Tetris = {};
    var grid = {width: 20, height: 20, columns: 14, rows: 20};
    var types = {I: "I", J: "J", L: "L", O: "O", S: "S", Z: "Z", T: "T"};
    var status = {A:0, B:1, C:2, D:3}
    var directions = {LEFT:'LEFT', RIGHT:'RIGHT', DOWN:'DOWN'};
    var outBoundsTypes = {BOTTOM:1, SIDE:2, OVERLAP:3};
    var levels = {0:10000, 1:500, 2:400, 3:200, 4:100};

    Tetris.grid = grid;
    Tetris.types = types;
    Tetris.status = status;
    Tetris.directions = directions;
    Tetris.outBoundsTypes = outBoundsTypes;
    Tetris.levels = levels;

    Tetris.init = function () {
        var blocks_arr = [];
        for (var r = 0; r < grid.rows; r++) {
            var rows = [];
            for (var c = 0; c < grid.columns; c++) {
                rows.push('<td id="id_' + c + '_' + r + '" class="grid" style="height:' + grid.height + 'px;width:' + grid.width + 'px"></td>');
            }
            blocks_arr.push('<tr class="row">' + rows.join('') + '</tr>');
        }
        var table = $('<table>' + blocks_arr.join('') + '</table>');
        $('#tetris').width(grid.width * grid.columns).height(grid.height * grid.rows).append(table);

        Tetris.addEvents();
    };

    Tetris.createBrick = function (type, status, positions) {
        var brick = {
            type:Tetris.types.I,//brick类型
            status: status,//brick状态
            positions:positions,//brick位置
            elements:[]//砖块元素
        };
        //if (type === types.J) {
        //
        //} else if (type === types.L) {
        //
        //} else if (type === types.O) {
        //
        //} else if (type === types.S) {
        //
        //} else if (type === types.Z) {
        //
        //} else if (type === types.T) {
        //
        //} else {
        //    //brick = Tetris._createI(type, status, positions);
        //    if(status === Tetris.status.A || status === Tetris.status.C){
        //
        //    }else if(status === Tetris.status.B || status == Tetris.status.D){
        //
        //    }else{
        //        throw Error('No Such Status '+status);
        //    }
        //}

        return brick;
    };

    Tetris.renderBrick = function (brick) {
        var type = brick.type;
        if (type === types.J) {

        } else if (type === types.L) {

        } else if (type === types.O) {

        } else if (type === types.S) {

        } else if (type === types.Z) {

        } else if (type === types.T) {

        } else {
            return Tetris._renderI(brick);
        }
    };

    Tetris.rotateBrick = function(brick){
        var type = brick.type;
        if (type === types.J) {

        } else if (type === types.L) {

        } else if (type === types.O) {

        } else if (type === types.S) {

        } else if (type === types.Z) {

        } else if (type === types.T) {

        } else {
            Tetris._rotateI(brick);
        }
    };

    Tetris.move = function(brick, direction){
        var direction = direction || directions.DOWN;
        var coordinates = brick.coordinates;
        var position = brick.position;
        var new_coordinates = $.extend(true, [], coordinates);
        if (direction === directions.LEFT) {
            for(var i = 0; i < new_coordinates.length; i++) new_coordinates[i][0]--;
        }else if(direction === directions.RIGHT){
            for(var i = 0; i < new_coordinates.length; i++) new_coordinates[i][0]++;
        }else{
            for(var i = 0; i < new_coordinates.length; i++) new_coordinates[i][1]++;
        }
        //如果越界，则不执行该操作
        var outBoundsType = Tetris._isOutBounds(new_coordinates, position);
        if(outBoundsType) return outBoundsType;
        brick.coordinates = new_coordinates;
        Tetris.renderBrick(brick);
        return brick;
    };

    Tetris.addEvents = function(){
        var UP = 38, DOWN=40, LEFT=37, RIGHT=39;
        $(document).keydown(function(event){
            if(!Tetris.current.brick) return;
            if(event.which === UP){
                Tetris.rotateBrick(Tetris.current.brick);
            }else if(event.which === DOWN){
                Tetris.move(Tetris.current.brick, Tetris.directions.DOWN);
            }else if(event.which === LEFT){
                Tetris.move(Tetris.current.brick, Tetris.directions.LEFT);
            }else if(event.which === RIGHT){
                Tetris.move(Tetris.current.brick, Tetris.directions.RIGHT);
            }else{

            }
        });
    };

    Tetris.start = function(){
        var level = Tetris.levels[0];
        console.log('level:'+level);
        var startPositions = [Tetris.grid.columns/2, -3];
        Tetris.current = {};

        setInterval(function(){
            if(!Tetris.current.brick){
                Tetris.current.brick = Tetris.createBrick(Tetris.I, Tetris.status.A, startPositions);
                Tetris.renderBrick(Tetris.current.brick);
            }
            var outBoundType = Tetris.move(Tetris.current.brick, Tetris.directions.DOWN);
            console.log('outBoundType: '+outBoundType);
            if(outBoundType === Tetris.outBoundsTypes.BOTTOM || outBoundType == Tetris.outBoundsTypes.OVERLAP){
                Tetris.current.brick = null;
            }
        }, level);
    };

    //这里需要返回越界类型，因为左右越界只是简单的不需变换该操作而已，但是底部越界则说明已经到底底部，需要出现下一个brick了
    Tetris._isOutBounds = function(coordinates, position){

        //变形之后，如果超出下边界，则不执行该变形操作
        var maxY = Math.max(position[1]+coordinates[0][1], position[1]+coordinates[1][1], position[1]+coordinates[2][1], position[1]+coordinates[3][1]);

        if(maxY >(Tetris.grid.rows - 1)) return Tetris.outBoundsTypes.BOTTOM;

        //变形之后，如果左右两边有任何超出左右边界，则不执行该变形操作
        var maxX = Math.max(position[0]+coordinates[0][0], position[0]+coordinates[1][0], position[0]+coordinates[2][0], position[0]+coordinates[3][0]);
        var minX = Math.min(position[0]+coordinates[0][0], position[0]+coordinates[1][0], position[0]+coordinates[2][0], position[0]+coordinates[3][0]);
        if(maxX >(Tetris.grid.columns - 1) || minX < 0) return Tetris.outBoundsTypes.SIDE;

        //左边已经有brick占位了
        //选出每一行中坐左边那个元素
        var leftCoordinates = getLeftCoordinates(coordinates);
        console.log('leftCoordinates: '+leftCoordinates);
        //右边已经有brick占位了
        //选出每一行中最右边那个元素

        ////变形之后，如果左右2边或者下面都已经有brick占位了，那么也不能执行该操作
        //if($('#id_'+(position[0]+coordinates[0][0]) + '_'+ (position[0]+coordinates[0][1])).hasClass('block')||
        //    $('#id_'+(position[0]+coordinates[1][0]) + '_'+ (position[0]+coordinates[1][1])).hasClass('block')||
        //    $('#id_'+(position[0]+coordinates[2][0]) + '_'+ (position[0]+coordinates[2][1])).hasClass('block')||
        //    $('#id_'+(position[0]+coordinates[3][0]) + '_'+ (position[0]+coordinates[3][1])).hasClass('block')){
        //    return Tetris.outBoundsTypes.OVERLAP;
        //}
        return false;
    };

    function getLeftCoordinates(coordinates){
        var leftCoordinates = [coordinates[0]];
        for(var i = 1; i < coordinates.length; i++){
            if(coordinates[i][1] == coordinates[i-1][1]){
                continue;
            }else{
                leftCoordinates.push(coordinates[i]);
            }
        }
        return leftCoordinates;
    }
    function getRightCoordinates(coordinates){}
    function getBottomCoordinates(coordinates){}
    window.Tetris = Tetris;
})(window, jQuery);